import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/services/auth/auth.service';
import UserAuth from 'src/app/models/user-auth';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  burgerOpen = false;

  get loggedIn(): boolean {
    return this.authService.loggedIn;
  }

  get userAuth(): UserAuth {
    return this.authService.userAuth;
  }

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  logOut() {
    this.authService.logOut();
    this.router.navigateByUrl('');
  }
}
