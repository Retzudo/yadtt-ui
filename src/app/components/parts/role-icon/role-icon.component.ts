import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-role-icon',
  templateUrl: './role-icon.component.html',
  styleUrls: ['./role-icon.component.scss']
})
export class RoleIconComponent {
  @Input() role: string;
  @Input() size = 24;

  get roleName() {
    if (this.role === 'dd') {
      return 'Damage Dealer';
    }

    return this.role;
  }
}
