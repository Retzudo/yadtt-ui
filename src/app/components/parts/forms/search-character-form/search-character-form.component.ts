import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, switchMap, tap } from 'rxjs/operators';
import { of } from 'rxjs';

import { CharacterService } from '../../../../services/character/character.service';
import Character from '../../../../models/character';

@Component({
  selector: 'app-search-character-form',
  templateUrl: './search-character-form.component.html',
  styleUrls: ['./search-character-form.component.scss']
})
export class SearchCharacterFormComponent implements OnInit {
  @Input() faction: string;
  @Input() guild: string;
  @Output() foundCharacters = new EventEmitter<Character[]>();
  @Output() isLoading = new EventEmitter<boolean>();

  form: FormGroup;
  loading = false;

  constructor(
    private fb: FormBuilder,
    private characterService: CharacterService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      search: ['']
    });

    this.form.get('search').valueChanges.pipe(
      debounceTime(500),
      tap(() =>  {
        this.loading = true;
        this.isLoading.emit(true);
      }),
      switchMap(term => {
        if (!term) {
          return of([]);
        }

        return this.characterService.list({
          faction: this.faction,
          guild: this.guild,
          name: term
        });
      }),
      tap(() =>  {
        this.loading = false;
        this.isLoading.emit(false);
      }),
    ).subscribe(characters => {
      this.foundCharacters.emit(characters);
    });
  }

}
