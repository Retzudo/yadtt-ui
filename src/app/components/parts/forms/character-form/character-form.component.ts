import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import Character from '../../../../models/character';

@Component({
  selector: 'app-character-form',
  templateUrl: './character-form.component.html',
  styleUrls: ['./character-form.component.scss']
})
export class CharacterFormComponent implements OnInit {
  @Output() submit = new EventEmitter<Character>();

  private raceClassCombinations = {
    dwarf: ['hunter', 'paladin', 'priest', 'rogue', 'warrior'],
    gnome: ['mage', 'rogue', 'warlock', 'warrior'],
    human: ['mage', 'paladin', 'priest', 'rogue', 'warrior'],
    nightelf: ['druid', 'hunter', 'priest', 'rogue', 'warrior'],
    forsaken: ['mage', 'priest', 'rogue', 'warlock', 'warrior'],
    orc: ['hunter', 'rogue', 'shaman', 'warlock', 'warrior'],
    tauren: ['druid', 'hunter', 'shaman', 'warrior'],
    troll: ['hunter', 'mage', 'priest', 'rogue', 'shaman', 'warrior'],
  };

  private classRoleCombinations = {
    druid: ['tank', 'healer', 'dd'],
    hunter: ['dd'],
    mage: ['dd'],
    paladin: ['tank', 'healer', 'dd'],
    priest: ['healer', 'dd'],
    rogue: ['dd'],
    shaman: ['healer', 'dd'],
    warlock: ['dd'],
    warrior: ['tank', 'dd']
  };

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  get availableClasses(): string[] {
    const raceControl = this.form.get('race');
    const classControl = this.form.get('class');
    const chosenRace = raceControl.value;
    classControl.enable();

    if (!chosenRace) {
      classControl.disable();
      return [];
    }

    return this.raceClassCombinations[chosenRace];
  }

  get availableRoles(): string[] {
    const classControl = this.form.get('class');
    const roleControl = this.form.get('role');
    const chosenClass = classControl.value;
    roleControl.enable();

    if (!chosenClass) {
      roleControl.disable();
      return [];
    }

    const combinations = this.classRoleCombinations[chosenClass];

    if (combinations.length === 1) {
      // Set the role automatically if there's only one.
      this.form.get('role').setValue(combinations[0]);
    }

    return combinations;
  }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      race: ['', Validators.required],
      gender: ['', Validators.required],
      class: ['', Validators.required],
      role: ['', Validators.required],
      level: [60, Validators.required],
    });
  }

  onSubmit(event) {
    event.preventDefault();

    if (this.form.valid) {
      this.submit.emit(Object.assign(
        new Character(),
        this.form.value
      ));
    }
  }
}
