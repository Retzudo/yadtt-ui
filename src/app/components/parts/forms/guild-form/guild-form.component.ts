import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import Guild from "../../../../models/guild";

@Component({
  selector: 'app-guild-form',
  templateUrl: './guild-form.component.html',
  styleUrls: ['./guild-form.component.scss']
})
export class GuildFormComponent implements OnInit {
  @Output() submit = new EventEmitter<Guild>();

  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      name: ['', Validators.required],
      realm: ['', Validators.required],
      faction: ['', Validators.required]
    })
  }

  onSubmit(event) {
    event.preventDefault();

    this.submit.emit(Object.assign(
      new Guild(),
      this.form.value
    ));
  }
}
