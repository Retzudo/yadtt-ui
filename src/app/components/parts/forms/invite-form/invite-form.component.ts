import { Component, EventEmitter, Input, Output } from '@angular/core';

import Character from 'src/app/models/character';

@Component({
  selector: 'app-invite-form',
  templateUrl: './invite-form.component.html',
  styleUrls: ['./invite-form.component.scss']
})
export class InviteFormComponent {
  @Input() guildId: string;
  @Input() faction: string;
  @Output() invite = new EventEmitter<string>();

  characters: Character[] = [];
  loading = false;

  onInvite(event, characterId: string) {
    event.preventDefault();

    this.invite.emit(characterId);
  }
}
