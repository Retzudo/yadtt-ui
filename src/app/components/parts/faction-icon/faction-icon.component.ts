import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-faction-icon',
  templateUrl: './faction-icon.component.html',
  styleUrls: ['./faction-icon.component.scss']
})
export class FactionIconComponent {
  @Input() faction: string;
  @Input() size = 24;
}
