import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-race-icon',
  templateUrl: './race-icon.component.html',
  styleUrls: ['./race-icon.component.scss']
})
export class RaceIconComponent {
  @Input() race: string;
  @Input() gender: string;
  @Input() size = 24;
}
