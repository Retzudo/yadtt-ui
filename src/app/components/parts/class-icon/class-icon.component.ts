import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-class-icon',
  templateUrl: './class-icon.component.html',
  styleUrls: ['./class-icon.component.scss']
})
export class ClassIconComponent {
  @Input() class: string;
  @Input() size = 24;
}
