import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import Guild from '../../../../models/guild';
import { AuthService } from '../../../../services/auth/auth.service';

@Component({
  selector: 'app-guild',
  templateUrl: './guild-details.component.html',
  styleUrls: ['./guild-details.component.scss']
})
export class GuildDetailsComponent implements OnInit {
  @Input() guild: Guild;

  constructor(private activatedRoute: ActivatedRoute, private authService: AuthService) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.guild = data.guild;
    });
  }

  get userCanManage(): boolean {
    return this.authService.userAuth.canManageGuild(this.guild.id);
  }
}
