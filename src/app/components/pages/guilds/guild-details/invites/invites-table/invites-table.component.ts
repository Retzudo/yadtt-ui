import { Component, Input } from '@angular/core';

import Invite from "../../../../../../models/invite";

@Component({
  selector: 'app-invites-table',
  templateUrl: './invites-table.component.html',
  styleUrls: ['./invites-table.component.scss']
})
export class InvitesTableComponent {
  @Input() invites: Invite[];
}
