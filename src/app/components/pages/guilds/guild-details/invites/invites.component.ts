import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

import Invite from "../../../../../models/invite";

@Component({
  selector: 'app-invites',
  templateUrl: './invites.component.html',
  styleUrls: ['./invites.component.scss']
})
export class InvitesComponent implements OnInit {
  invites: Invite[];

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.invites = data.invites;
    });
  }
}
