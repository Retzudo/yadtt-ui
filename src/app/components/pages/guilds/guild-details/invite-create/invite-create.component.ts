import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import Invite from "../../../../../models/invite";
import { InviteService } from "../../../../../services/guild/invite.service";
import Guild from 'src/app/models/guild';

@Component({
  selector: 'app-invite-create',
  templateUrl: './invite-create.component.html',
  styleUrls: ['./invite-create.component.scss']
})
export class InviteCreateComponent implements OnInit {
  guild: Guild;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private inviteService: InviteService) { }

  ngOnInit(): void {
    this.activatedRoute.parent.data.subscribe(data => {
      this.guild = data.guild;
    });
  }

  onSendInvite(characterId: string) {
    const invite = new Invite(this.guild.id, characterId);

    this.inviteService.create(invite).subscribe();
  }
}
