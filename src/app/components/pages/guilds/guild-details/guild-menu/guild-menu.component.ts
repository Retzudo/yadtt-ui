import { Component, Input } from '@angular/core';

import Guild from '../../../../../models/guild';

@Component({
  selector: 'app-guild-menu',
  templateUrl: './guild-menu.component.html',
  styleUrls: ['./guild-menu.component.scss']
})
export class GuildMenuComponent {
  @Input() guild: Guild;
  @Input() canManage: boolean;
}
