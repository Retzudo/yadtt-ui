import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import Guild from '../../../../../models/guild';
import Character from '../../../../../models/character';
import { GuildService } from '../../../../../services/guild/guild.service';
import { CharacterService } from '../../../../../services/character/character.service';

@Component({
  selector: 'app-award-dkp',
  templateUrl: './award-dkp.component.html',
  styleUrls: ['./award-dkp.component.scss']
})
export class AwardDkpComponent implements OnInit {
  guild: Guild;
  form: FormGroup;
  foundCharacters: Character[] = [];
  recentEvents: {dkp: number; description: string;}[] = [];
  characters: Set<Character> = new Set<Character>();
  searching = false;
  loading = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private guildService: GuildService,
    private characterService: CharacterService,
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      dkp: ['', Validators.required],
      description: ['', Validators.required]
    });

    this.activatedRoute.parent.data.subscribe(data => {
      this.guild = data.guild;
    });

    this.restoreCachedCharacters();
  }

  private restoreCachedCharacters() {
    const characterIdsString = localStorage.getItem(`yadtt:guilds:${this.guild.id}:cachedCharacters`);

    if (!characterIdsString) {
      return;
    }

    const characterIds = characterIdsString.split(',').map(s => s.trim());

    this.characterService.list({id: characterIds}).subscribe(characters => {
      this.characters.clear();
      characters.map(c => this.characters.add(c));
      this.cacheCharacters(this.characters);
    });
  }

  private cacheCharacters(characters: Set<Character>) {
    const string = Array.from(characters).map(c => c.id).join(',');
    localStorage.setItem(`yadtt:guilds:${this.guild.id}:cachedCharacters`, string);
  }

  onDkpSubmit(event) {
    event.preventDefault();
    const { dkp, description } = this.form.value;
    this.loading = true;

    this.guildService.awardDkp(
      this.guild.id,
      Array.from(this.characters).map(c => c.id),
      dkp,
      description
    ).subscribe(() => {
      this.loading = false;
      this.recentEvents.push({ dkp, description });
      this.form.patchValue({
        description: ''
      });
    });
  }

  addCharacter(character: Character) {
    this.characters.add(character);
    this.foundCharacters = this.foundCharacters.filter(c => c.id !== character.id);

    this.cacheCharacters(this.characters);
  }

  removeCharacter(character: Character) {
    this.characters.delete(character);

    this.cacheCharacters(this.characters);
  }

  removeAll(event) {
    event.preventDefault();
    this.characters.clear();

    this.cacheCharacters(this.characters);
  }
}
