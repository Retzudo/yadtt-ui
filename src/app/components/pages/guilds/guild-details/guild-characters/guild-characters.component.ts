import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import Character from "../../../../../models/character";

@Component({
  selector: 'app-characters',
  templateUrl: './guild-characters.component.html',
  styleUrls: ['./guild-characters.component.scss']
})
export class GuildCharactersComponent implements OnInit {
  characters: Character[];

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.characters = data.members;
    });
  }

}
