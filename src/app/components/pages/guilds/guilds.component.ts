import { Component, OnInit } from '@angular/core';

import Guild from '../../../models/guild';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-guilds',
  templateUrl: './guilds.component.html',
  styleUrls: ['./guilds.component.scss']
})
export class GuildsComponent implements OnInit {
  guilds: Guild[];

  constructor(private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.guilds = data.guilds;
    });
  }

}
