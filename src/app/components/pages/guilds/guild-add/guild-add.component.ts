import { Component } from '@angular/core';
import Guild from "../../../../models/guild";
import {GuildService} from "../../../../services/guild/guild.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-guild-add',
  templateUrl: './guild-add.component.html',
  styleUrls: ['./guild-add.component.scss']
})
export class GuildAddComponent {
  constructor(private guildService: GuildService, private router: Router) { }

  onSubmit(guild: Guild) {
    if (guild instanceof Event) {
      return;
    }

    this.guildService.create(guild).subscribe(guild => {
      this.router.navigateByUrl(`/guilds/${guild.id}`);
    });
  }
}
