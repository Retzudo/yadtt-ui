import { Component, Input, OnInit } from '@angular/core';

import Guild from '../../../../models/guild';

@Component({
  selector: 'app-guild-card',
  templateUrl: './guild-card.component.html',
  styleUrls: ['./guild-card.component.scss']
})
export class GuildCardComponent implements OnInit {
  @Input() guild: Guild;

  constructor() { }

  ngOnInit() {
  }

}
