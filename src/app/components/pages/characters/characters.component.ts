import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import Character from '../../../models/character';
import Invite from "../../../models/invite";
import { InviteService } from "../../../services/guild/invite.service";

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  characters: Character[];
  invites: Invite[];

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private inviteService: InviteService
  ) { }

  ngOnInit() {
    this.activatedRoute.data.subscribe(data => {
      this.characters = data.characters;
      this.invites = data.invites;
    })
  }

  inviteForCharacter(characterId: string): Invite {
    return this.invites.find(i => i.invitee === characterId);
  }

  acceptInvite(invite: Invite) {
    if (invite instanceof Event) {
      return;
    }

    this.inviteService.accept(invite.id).subscribe(() => {
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigateByUrl('/characters/list');
    })
  }
}
