import { Component, OnInit } from '@angular/core';

import Character from '../../../../models/character';
import { CharacterService } from '../../../../services/character/character.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-character-add',
  templateUrl: './character-add.component.html',
  styleUrls: ['./character-add.component.scss']
})
export class CharacterAddComponent implements OnInit {

  constructor(private characterService: CharacterService, private router: Router) { }

  ngOnInit() {
  }

  onSubmit(character: Character) {
    if (character instanceof Event) {
      return;
    }

    this.characterService.create(character).subscribe(() => {
      this.router.navigateByUrl('/characters');
    });
  }
}
