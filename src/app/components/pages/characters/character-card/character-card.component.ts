import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';

import Character from '../../../../models/character';
import Event from '../../../../models/event';
import { EventService } from '../../../../services/character/event.service';
import Invite from "../../../../models/invite";

@Component({
  selector: 'app-character-card',
  templateUrl: './character-card.component.html',
  styleUrls: ['./character-card.component.scss']
})
export class CharacterCardComponent implements OnInit {
  @Input() character: Character;
  @Input() invite: Invite = null;

  @Output() acceptInvite = new EventEmitter<Invite>();

  lastEvents$: Observable<Event[]>;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    if (this.character.guild) {
      this.lastEvents$ = this.eventService.list(this.character.id, this.character.guild.id);
    }
  }

  onAcceptInvite(event) {
    event.preventDefault();

    this.acceptInvite.emit(this.invite);
  }
}
