import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/pages/login/login.component';
import { HomeComponent } from './components/pages/home/home.component';
import { CharactersComponent } from './components/pages/characters/characters.component';
import { GuildsComponent } from './components/pages/guilds/guilds.component';
import { AuthGuard } from './auth.guard';
import GuildsResolver from './resolvers/guilds/guilds-resolver';
import CharactersResolver from './resolvers/characters/characters-resolver';
import { GuildDetailsComponent } from './components/pages/guilds/guild-details/guild-details.component';
import GuildResolver from './resolvers/guilds/guild-resolver';
import { ManageGuard } from './manage.guard';
import { GuildCharactersComponent } from './components/pages/guilds/guild-details/guild-characters/guild-characters.component';
import { CharacterAddComponent } from './components/pages/characters/character-add/character-add.component';
import { GuildAddComponent } from './components/pages/guilds/guild-add/guild-add.component';
import GuildMembersResolver from './resolvers/guilds/guild-members-resolver';
import { InvitesComponent } from './components/pages/guilds/guild-details/invites/invites.component';
import InvitesResolver from './resolvers/guilds/invites-resolver';
import { InviteCreateComponent } from './components/pages/guilds/guild-details/invite-create/invite-create.component';
import PersonalInvitesResolver from './resolvers/characters/personal-invites-resolver';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';
import { AwardDkpComponent } from './components/pages/guilds/guild-details/award-dkp/award-dkp.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'characters',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: CharactersComponent,
        runGuardsAndResolvers: 'always',
        resolve: {
          characters: CharactersResolver,
          invites: PersonalInvitesResolver
        },
      },
      {
        path: 'add',
        component: CharacterAddComponent
      }
    ]
  },
  {
    path: 'guilds',
    canActivate: [AuthGuard],
    runGuardsAndResolvers: 'always',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list'
      },
      {
        path: 'list',
        component: GuildsComponent,
        resolve: {
          guilds: GuildsResolver
        }
      },
      {
        path: 'add',
        component: GuildAddComponent
      },
      {
        path: ':id',
        component: GuildDetailsComponent,
        resolve: {
          guild: GuildResolver
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'members'
          },
          {
            path: 'members',
            component: GuildCharactersComponent,
            resolve: {
              members: GuildMembersResolver
            }
          },
          {
            path: 'invites',
            component: InvitesComponent,
            canActivate: [ManageGuard],
            resolve: {
              invites: InvitesResolver
            }
          },
          {
            path: 'award-dkp',
            component: AwardDkpComponent,
            canActivate: [ManageGuard]
          },
          {
            path: 'invites/create',
            component: InviteCreateComponent,
            canActivate: [ManageGuard]
          }
        ]
      }
    ]
  },
  {
    path: '**',
    component: NotFoundComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
