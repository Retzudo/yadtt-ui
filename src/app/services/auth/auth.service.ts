import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as jwtDecode from 'jwt-decode';

import UserAuth from '../../models/user-auth';
import { BASE_URL } from '../../config';
import { map, tap } from 'rxjs/operators';

interface TokenResponse {
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private TOKEN_NAME = 'yadtt.auth';

  constructor(private http: HttpClient) { }

  public get token(): string {
    return localStorage.getItem(this.TOKEN_NAME);
  }

  public get tokenExpired(): boolean {
    const { userAuth } = this;

    if (!userAuth) {
      return true;
    }

    return new Date() > userAuth.expiryDate;
  }

  private saveToken(token: string) {
    localStorage.setItem(this.TOKEN_NAME, token);
  }

  private deleteToken() {
    localStorage.removeItem(this.TOKEN_NAME);
  }

  private userAuthFromToken(): UserAuth {
    if (!this.token) {
      return null;
    }

    const payload = jwtDecode(this.token);

    return Object.assign(new UserAuth(), {
      ...payload,
      id: payload.user_id,
      manageableGuilds: payload.manageableGuilds.map(id => `${id}`)
    });
  }

  public logIn(username: string, password: string): Observable<UserAuth> {
    return this.http.post<TokenResponse>(`${BASE_URL}/auth/`, {username, password}).pipe(
      tap(response => this.saveToken(response.token)),
      map(() => this.userAuthFromToken())
    );
  }

  public logOut() {
    this.deleteToken();
  }

  public get loggedIn(): boolean {
    if (!this.token) {
      return false;
    }

    if (!this.tokenExpired) {
      return true;
    }

    this.logOut();
    return false;
  }

  public get userAuth(): UserAuth {
    return this.userAuthFromToken();
  }
}
