import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BASE_URL } from '../../config';
import Guild from '../../models/guild';

@Injectable({
  providedIn: 'root'
})
export class GuildService {
  constructor(private http: HttpClient) { }

  public list(): Observable<Guild[]> {
    return this.http.get<Guild[]>(`${BASE_URL}/guilds/`);
  }

  public get(id: string): Observable<Guild> {
    return this.http.get<Guild>(`${BASE_URL}/guilds/${id}/`);
  }

  public create(guild: Guild): Observable<Guild> {
    return this.http.post<Guild>(`${BASE_URL}/guilds/`, guild);
  }

  public update(id: string, guild: Guild): Observable<Guild> {
    return this.http.put<Guild>(`${BASE_URL}/guilds/${id}/`, guild);
  }

  public delete(id: string) {
    return this.http.delete(`${BASE_URL}/guilds/${id}/`);
  }

  public awardDkp(guildId: string, characterIds: string[], dkp: number, description: string) {
    return this.http.post(`${BASE_URL}/guilds/${guildId}/awarddkp/`, {
      dkp,
      description,
      characters: characterIds
    });
  }
}
