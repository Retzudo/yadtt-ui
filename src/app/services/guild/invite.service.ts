import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

import Invite from "../../models/invite";
import {BASE_URL} from "../../config";
import { cleanParams, GenericFilter } from '../util';

export interface InviteFilter extends GenericFilter {
  guild?: string;
}

@Injectable({
  providedIn: 'root'
})
export class InviteService {

  constructor(private http: HttpClient) { }

  list(inviteFilter?: InviteFilter): Observable<Invite[]> {
    return this.http.get<Invite[]>(`${BASE_URL}/invites/`, {
      params: cleanParams(inviteFilter)
    });
  }

  create(invite: Invite): Observable<Invite> {
    return this.http.post<Invite>(`${BASE_URL}/invites/`, invite);
  }

  accept(inviteId: string): Observable<any> {
    return this.http.post(`${BASE_URL}/invites/${inviteId}/accept/`, {});
  }
}
