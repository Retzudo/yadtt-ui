export interface GenericFilter {
  [key: string]: any;
}

export function cleanParams(params: GenericFilter): {[key: string]: string} {
  if (!params) {
    return null;
  }

  const newObject = {};

  for (const key in params) {
    if (params[key]) {
      newObject[key] = params[key];
    }
  }

  return newObject;
}
