import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BASE_URL } from '../../config';
import Event from '../../models/event';


@Injectable({
  providedIn: 'root'
})
export class EventService {
  constructor(private http: HttpClient) { }

  public list(characterId: string = null, guildId: string = null): Observable<Event[]> {
    return this.http.get<Event[]>(`${BASE_URL}/events`, {
      params: {
        character: characterId,
        guild: guildId
      }
    });
  }
}
