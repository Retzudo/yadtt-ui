import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BASE_URL } from '../../config';
import Character from '../../models/character';
import { cleanParams, GenericFilter } from "../util";

export interface CharacterFilter extends GenericFilter {
  user?: string;
  guild?: string;
  faction?: string;
  name?: string;
  noguild?: boolean;
  id?: string | string[];
}

@Injectable({
  providedIn: 'root'
})
export class CharacterService {
  constructor(private http: HttpClient) { }

  public list(characterFilter: CharacterFilter): Observable<Character[]> {
    return this.http.get<Character[]>(`${BASE_URL}/characters/`, {
      params: cleanParams(characterFilter)
    });
  }

  public get(id: string): Observable<Character> {
    return this.http.get<Character>(`${BASE_URL}/characters/${id}/`);
  }

  public create(character: Character): Observable<Character> {
    return this.http.post<Character>(`${BASE_URL}/characters/`, character);
  }

  public update(id: string, character: Character): Observable<Character> {
    return this.http.put<Character>(`${BASE_URL}/characters/${id}/`, character);
  }

  public delete(id: string) {
    return this.http.delete(`${BASE_URL}/characters/${id}/`);
  }
}
