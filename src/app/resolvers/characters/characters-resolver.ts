import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import Character from '../../models/character';
import { CharacterService } from '../../services/character/character.service';
import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export default class CharactersResolver implements Resolve<Character[]> {
  constructor(private characterService: CharacterService, private authService: AuthService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Character[]> {
    const userId = this.authService.userAuth.id;

    return this.characterService.list({user: userId});
  }
}
