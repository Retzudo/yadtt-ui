import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import Invite from "../../models/invite";
import { InviteService } from "../../services/guild/invite.service";

@Injectable({
  providedIn: 'root'
})
export default class PersonalInvitesResolver implements Resolve<Invite[]> {
  constructor(private inviteService: InviteService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Invite[]> {
    return this.inviteService.list();
  }
}
