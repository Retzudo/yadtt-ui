import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";
import Character from "../../models/character";
import {Observable} from "rxjs";
import {CharacterService} from "../../services/character/character.service";
import {Injectable} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export default class GuildMembersResolver implements Resolve<Character[]> {
  constructor(private characterService: CharacterService) {

  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Character[]> {
    const guildId = route.parent.params.id;

    return this.characterService.list({guild: guildId});
  }

}
