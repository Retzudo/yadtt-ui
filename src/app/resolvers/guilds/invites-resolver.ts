import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from "@angular/router";

import Invite from "../../models/invite";
import {Observable} from "rxjs";
import {InviteService} from "../../services/guild/invite.service";

@Injectable({
  providedIn: 'root'
})
export default class InvitesResolver implements Resolve<Invite[]> {
  constructor(private inviteService: InviteService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Invite[]> {
    const guildId = route.parent.params.id;

    return this.inviteService.list({guild: guildId})
  }
}
