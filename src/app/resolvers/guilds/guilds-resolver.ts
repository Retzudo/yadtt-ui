import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import Guild from '../../models/guild';
import { GuildService } from '../../services/guild/guild.service';

@Injectable({
  providedIn: 'root'
})
export default class GuildsResolver implements Resolve<Guild[]> {
  constructor(private guildService: GuildService) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Guild[]> {
    return this.guildService.list();
  }
}
