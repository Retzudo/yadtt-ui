export default class UserAuth {
  readonly id: string;
  readonly username: string;
  readonly email: string;
  readonly exp: number;
  readonly manageableGuilds: string[];

  public canManageGuild(guildId: string): boolean {
    return this.manageableGuilds.includes(`${guildId}`);
  }

  public get expiryDate(): Date {
    return new Date(this.exp * 1000);
  }
}
