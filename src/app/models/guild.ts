export default class Guild {
  readonly id: string;
  readonly memberCount: number;
  name: string;
  faction: string;
  realm: string;
}
