import Guild from './guild';

export default class Character {
  readonly id: string;
  name: string;
  race: string;
  gender: string;
  class: string;
  role: string;
  level: number;
  totalDkp: number;
  readonly guild: Guild;
}
