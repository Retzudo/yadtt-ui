export default class Invite {
  readonly id: string;
  readonly guild: string;
  inviter: string;
  invitee: string;
  created: Date;

  constructor(guildId: string, characterId: string) {
    this.guild = guildId;
    this.invitee = characterId;
  }
}
