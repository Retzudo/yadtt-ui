import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingBarHttpClientModule } from "@ngx-loading-bar/http-client";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/pages/login/login.component';
import { NavbarComponent } from './components/parts/navbar/navbar.component';
import { HomeComponent } from './components/pages/home/home.component';
import { CharactersComponent } from './components/pages/characters/characters.component';
import { GuildDetailsComponent } from './components/pages/guilds/guild-details/guild-details.component';
import { GuildsComponent } from './components/pages/guilds/guilds.component';
import AuthInterceptor from './auth.interceptor';
import { AuthService } from './services/auth/auth.service';
import { CharacterCardComponent } from './components/pages/characters/character-card/character-card.component';
import { GuildCardComponent } from './components/pages/guilds/guild-card/guild-card.component';
import { CharacterTableComponent } from './components/pages/guilds/guild-details/guild-characters/character-table/character-table.component';
import { RaceIconComponent } from './components/parts/race-icon/race-icon.component';
import { ClassIconComponent } from './components/parts/class-icon/class-icon.component';
import { RoleIconComponent } from './components/parts/role-icon/role-icon.component';
import { GuildRunsComponent } from './components/pages/guilds/guild-details/guild-runs/guild-runs.component';
import { GuildCharactersComponent } from './components/pages/guilds/guild-details/guild-characters/guild-characters.component';
import { CharacterAddComponent } from './components/pages/characters/character-add/character-add.component';
import { CharacterFormComponent } from './components/parts/forms/character-form/character-form.component';
import { GuildAddComponent } from './components/pages/guilds/guild-add/guild-add.component';
import { GuildFormComponent } from './components/parts/forms/guild-form/guild-form.component';
import { FactionIconComponent } from './components/parts/faction-icon/faction-icon.component';
import { InvitesComponent } from './components/pages/guilds/guild-details/invites/invites.component';
import { InviteCreateComponent } from './components/pages/guilds/guild-details/invite-create/invite-create.component';
import { InviteFormComponent } from './components/parts/forms/invite-form/invite-form.component';
import { InvitesTableComponent } from './components/pages/guilds/guild-details/invites/invites-table/invites-table.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';
import { AwardDkpComponent } from './components/pages/guilds/guild-details/award-dkp/award-dkp.component';
import { GuildMenuComponent } from './components/pages/guilds/guild-details/guild-menu/guild-menu.component';
import { SearchCharacterFormComponent } from './components/parts/forms/search-character-form/search-character-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    HomeComponent,
    CharactersComponent,
    GuildDetailsComponent,
    GuildsComponent,
    CharacterCardComponent,
    GuildCardComponent,
    CharacterTableComponent,
    RaceIconComponent,
    ClassIconComponent,
    RoleIconComponent,
    GuildRunsComponent,
    GuildCharactersComponent,
    CharacterAddComponent,
    CharacterFormComponent,
    GuildAddComponent,
    GuildFormComponent,
    FactionIconComponent,
    CharacterTableComponent,
    InvitesComponent,
    InviteCreateComponent,
    InviteFormComponent,
    InvitesTableComponent,
    NotFoundComponent,
    AwardDkpComponent,
    GuildMenuComponent,
    SearchCharacterFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    LoadingBarHttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
      deps: [AuthService]
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
